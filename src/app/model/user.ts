import {Service} from './service';
import {Role} from './role';

export class User {

  get roleData(): any {
    return this._roleData;
  }

  set roleData(value: any) {
    this._roleData = value;
  }
  get userNumber(): number {
    return this._userNumber;
  }

  set userNumber(value: number) {
    this._userNumber = value;
  }

  get services(): Service[] {
    return this._services;
  }

  set services(value: Service[]) {
    this._services = value;
  }

  get roles(): string[] {
    return this._roles;
  }

  set roles(value: string[]) {
    this._roles = value;
  }

  get sex(): string {
    return this._sex;
  }

  set sex(value: string) {
    this._sex = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  public get firstName(): string {
    return this._firstName;
  }

  public set firstName(value: string) {
    this._firstName = value;
  }

  public get lastName(): string {
    return this._lastName;
  }

  public set lastName(value: string) {
    this._lastName = value;
  }

  public get email(): string {
    return this._email;
  }

  public set email(value: string) {
    this._email = value;
  }
  private _userNumber: number;
  private _firstName: string;
  private _lastName: string;
  private _email: string;
  private _services: Service[];
  private _roles: string[];
  private _sex: string;
  private _title: string;
  private _roleData: any;

  /*constructor(public _firstName: string, public _lastName: string, public _email: string) {
    this._firstName = _firstName;
    this._lastName = _lastName;
    this._email = _email;
  }*/
  constructor() {}
}
