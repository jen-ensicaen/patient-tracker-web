import {User} from './user';

export class Service {

  private _patientlist: User[];

  constructor(public serviceNumber: number, public serviceName: string) {
    this.serviceName = serviceName;
    this.serviceNumber = serviceNumber;
  }

  get patientlist(): User[] {
    return this._patientlist;
  }

  set patientlist(value: User[]) {
    this._patientlist = value;
  }
}
