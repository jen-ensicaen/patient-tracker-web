import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class PatientAddComponent implements OnInit {
  private apiUrl = 'https://pr2a-candellier.ecole.ensicaen.fr';
  private urlAddPatient = '/api/patient/register/{serviceId}';
  serviceId: number = 1;
  addPatientForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private http:Http
  ) {
    this.addPatientForm = formBuilder.group({
      'titleId': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'email': ['', Validators.required],
      'address': ['', Validators.required],
      'city': ['', Validators.required],
      'zipCode': ['', Validators.required],
      'socialSecurityNumber': ['', Validators.required],
      'serviceId': ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    // get form data
    let formdata = this.addPatientForm.value;
    // format data for api
    let data = {
      patient: {
        title: {
          titleId: formdata.titleId
        },
        firstName: formdata.firstName,
        lastName: formdata.lastName,
        email: formdata.email,
        sex: (formdata.titleId == 1) ? 'M' : 'F'
      },
      patientInfo: {
        socialSecurityNumber: formdata.socialSecurityNumber,
        address: formdata.address,
        city: formdata.city,
        zipCode: formdata.zipCode
      }
    };
    console.log('sumbit', data);

    // send data to api
    let url = this.apiUrl + this.urlAddPatient.replace('{serviceId}', this.serviceId.toString());
    console.log('API url', url);
    let headers = new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')});
    let options = new RequestOptions({headers: headers});
    this.http.post(url, data, options).subscribe(response => {
      console.log('API response', response);
    })
  }

}
