import { Component, OnInit } from '@angular/core';

import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {User} from '../model/user';
import {Role} from '../model/role';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private apiUrl = 'https://pr2a-candellier.ecole.ensicaen.fr';
  private urlGetUser = '/api/user/{userId}';
  private userId = localStorage.getItem('userId');
  user = new User();
  show = false;

  constructor(private http: Http) { }

  ngOnInit() {
    /*this.user.firstName = 'Jane';
    this.user.lastName = 'Doe';*/
    this.getUser();


  }

  getUserAPI() {
    const url = this.apiUrl + this.urlGetUser.replace('{userId}', this.userId);
    console.log('API url', url);

    const headers = new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')});
    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options).map((data: Response) => data.json());
  }

  getRole(role: string) {
    if (role === 'manager') {
      this.show = true;
    }
  }

  getUser(): void {
    this.getUserAPI().subscribe(result => {
        this.user.firstName = result.firstName,
          this.user.lastName = result.lastName,
          this.user.email = result.email,
          this.user.sex = result.sex,
          this.user.title = result.title.label,
          this.user.roles = result.roles,
          this.show = (this.user.roles.indexOf('manager') !== -1);
      });
  }

}
