import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';

import 'rxjs/add/operator/map';
import { Router } from "@angular/router";

@Injectable()
export class AuthService {

  constructor(
    private http: Http,
    private router: Router
  ) { }

  loggedIn() {
    return tokenNotExpired();
  }

  authenticate(user: any) {
    const url = 'https://pr2a-candellier.ecole.ensicaen.fr/api/auth/token';

    // ==== Create request headers ====
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({
      headers: headers
    });

    // ==== make api post call ====
    return this.http
      .post(url, JSON.stringify(user), options)
      .map((data: Response) => data.json());
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['']);
  }
}
